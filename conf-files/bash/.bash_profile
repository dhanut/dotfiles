#
# ~/.bash_profile
#

# Cursor support on wayland
export WLR_NO_HARDWARE_CURSORS=1

export MOZ_ENABLE_WAYLAND=1


#Export custom paths
export PATH=$PATH:$HOME/scripts
export PATH=$PATH:$HOME/.cargo/bin

#export date variable
export DATE=$(date -I)

[[ -f ~/.bashrc ]] && . ~/.bashrc

[ "$(tty)" = "/dev/tty1" ] && exec sway
