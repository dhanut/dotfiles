#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

## Alias ##
alias ls='exa --long'
#alias cat='bat'
alias lf='joshuto'
alias sbash='source $HOME/.bashrc'
alias la='exa --long --all'
# alias topgrade='topgrade -cy'

PS1='[\u@\h \W]\$ '

# Configure starship
eval "$(starship init bash)"
alias penpot-desktop='~/.local/share/korbs-studio/penpot-desktop/app/Penpot\ *.AppImage'
alias penpot-desktop='~/.local/share/korbs-studio/penpot-desktop/app/Penpot\ *.AppImage'
